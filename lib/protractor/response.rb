module Protractor
  module Response
    # Create a Hashie::Mash with response-related methods
    # An example response in JSON format can be found here:
    #   https://gist.github.com/shreve/e36d8f0e98b2ce662ed8
    #
    # @params response_hash [Hashie::Mash] data returned from a Faraday request
    # @return [Hashie::Mash]
    def self.create(response_hash)
      data = response_hash["CRMDataSet"].dup rescue response_hash
      if data.is_a?(String) # Presumably server error
        raise Error, "There was an error performing that search"
      end
      if data.has_key?("Header") and !data["Header"]["ErrorMessage"].nil?
        raise Error, data["Header"]["ErrorMessage"]
      end
      data.extend(self)
      return data
    end

    # Get the list of contacts returned from a request
    #
    # @return [Array]
    def contacts
      contacts = self["Contacts"]
      if contacts.nil?
        []
      else
        contacts["Item"]
      end
    end

    # Get the list of invoices returned from a request
    #
    # @return [Array]
    def invoices
      invoices = self["Invoices"]
      if invoices.nil?
        []
      else
        invoices["Item"]
      end
    end

    # Get the list of vehicles returned from a request
    #
    # @return [Array]
    def vehicles
      vehicles = self["ServiceItems"]
      if vehicles.nil?
        []
      else
        vehicles["Item"].select{|it|it["Type"].to_s == "Vehicle"}
      end
    end
    attr_reader :pagination
    attr_reader :meta

    class Error < StandardError
    end
  end
end
