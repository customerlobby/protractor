require 'faraday_middleware'
Dir[File.expand_path('../../faraday/*.rb', __FILE__)].each{|f| require f}

module Protractor
  module Connection
    private

    def connection
      options = {
        :url => [domain, api_version, endpoint].join('/')
      }

      Faraday::Connection.new(options) do |connection|
        connection.use FaradayMiddleware::Mashify
        connection.use Faraday::Response::ParseXml,  content_type: /\bxml$/
        connection.use Faraday::Response::ParseJson, content_type: /\bjson$/
        connection.adapter(adapter)
      end
    end
  end
end
