module Protractor
  module Request

    # Perform an HTTP GET request
    #
    # @param options [Hash] get query parameters
    # @return [Hash]
    def get(options = {})
      raise ArgumentError, "startDate is required" unless options.has_key?(:startDate)
      options[:endDate] = Time.now unless options.has_key?(:endDate)
      request(:get, options)
    end

    # Get the URL that would be requested for the given options
    #
    # @param options [Hash] get query parameters
    # @return [String]
    def url_for(options = {})
      base = [Protractor.domain, Protractor.api_version, Protractor.endpoint].join('/')
      joint = base.include?('?') ? '&' : '?'
      base << joint << sanitize(options.merge(authentication_params)).to_query if options.any?
      base
    end

    private

    OPTIONS_WHITELIST = [:startDate, :endDate, :connectionId, :apiKey]

    # Perform an HTTP request
    def request(method, options)
      options.merge!(authentication_params)
      options = sanitize(options)
      response = connection.send(method) do |request|
        case method
        when :get
          request.url(url_for(options))
        when :post, :put
          request.headers['Content-Type'] = 'application/json'
          request.body = options.to_json unless options.empty?
          request.url('')
        end
      end

      Response.create(response.body)
    end

    # Format and sanitize the options before you send them off to Protractor
    #
    # @param options [Hash] request options to sanitize
    # @return [Hash]
    def sanitize(options)
      return if options.blank?
      safe_options = {}
      OPTIONS_WHITELIST.each do |field|
        safe_options[field] = format_field(options[field]) if options.has_key?(field)
      end
      safe_options
    end

    # Format the fields to a format that the Protractor likes
    # @param value [Array, Date, or String] value to be formatted
    # @return String
    def format_field(value)
      if value.instance_of?(Array)
        value.join(",")
      elsif value.respond_to?(:strftime)
        value.strftime("%Y-%m-%d")
      elsif value.instance_of?(String)
        value
      end
    end

    def authentication_params
      { connectionId: connection_id, apiKey: api_key }
    end
  end
end
