module Protractor
  class Client
    module Invoices
      # Retreive a list of invoices
      #
      # @param options [Hash] list of attributes to search with
      def invoices(options = {})
        get(options).invoices
      end
    end
  end
end
