module Protractor
  class Client
    module Contacts
      # Retreive a list of contacts
      #
      # @param options [Hash] list of attributes to search with
      def contacts(options = {})
        get(options).contacts
      end
    end
  end
end
