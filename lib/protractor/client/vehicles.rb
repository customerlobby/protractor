module Protractor
  class Client
    module Vehicles
      # Retreive a list of vehicles
      #
      # @param options [Hash] list of attributes to search with
      def vehicles(options = {})
        get(options).vehicles
      end
    end
  end
end
