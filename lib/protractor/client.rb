module Protractor
  class Client < API
    Dir[File.expand_path('../client/*.rb', __FILE__)].each{ |f| require f }

    include Contacts
    include Invoices
    include Vehicles
  end
end
