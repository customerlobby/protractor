module Protractor
  module Configuration

    VALID_OPTIONS_KEYS = [
      :adapter,
      :api_key,
      :api_version,
      :connection_id,
      :domain,
      :endpoint,
      :test_mode
    ].freeze

    # Use the default Faraday adapter.
    DEFAULT_ADAPTER = Faraday.default_adapter

    # By default use V1 of the API.
    DEFAULT_API_VERSION = '1.0'.freeze

    # Use the main api domain root
    DEFAULT_DOMAIN = 'https://integration.protractor.com/ExternalCRM'.freeze

    # By default use the main api URL.
    DEFAULT_ENDPOINT = 'GetCRMData.ashx'.freeze

    # By default don't use test mode
    DEFAULT_TEST_MODE = false

    # Convenience method to allow configuration options to be set in a block
    def configure
      yield self
    end

    # The currently set options for Protractor
    #
    # @return [Hash]
    def options
      VALID_OPTIONS_KEYS.inject({}) do |option, key|
        option.merge!(key => send(key))
      end
    end

    # When this module is extended, create methods for all available options
    # and reset the ones with default values.
    def self.extended(base)
      VALID_OPTIONS_KEYS.each do |option|
        base.class_eval """
          @@#{option} = nil
          def self.#{option}; @@#{option}; end
          def self.#{option}=(*args); @@#{option} = args.first; end
        """
        base.class_eval("def self.api_version; @@test_mode ? 'Test' : @@api_version; end")
      end
      base.reset
    end

    # Reset all configuration settings to default values.
    def reset
      self.api_version = DEFAULT_API_VERSION
      self.adapter     = DEFAULT_ADAPTER
      self.domain      = DEFAULT_DOMAIN
      self.endpoint    = DEFAULT_ENDPOINT
      self.test_mode   = DEFAULT_TEST_MODE
      true
    end
  end
end
