require File.expand_path('../request', __FILE__)
require File.expand_path('../response', __FILE__)
require File.expand_path('../connection', __FILE__)

module Protractor
  class API

    def initialize(options={})
      Configuration::VALID_OPTIONS_KEYS.each do |key|
        send("#{key}=", options[key]) if options.has_key?(key)
      end
    end

    def method_missing(method, *args, &block)
      Protractor.send(method, *args, &block)
    end

    include Request
    include Response
    include Connection
  end
end
