require 'faraday'
require 'faraday_middleware'
require 'active_support/all'
require 'protractor/version'

Dir[File.expand_path('../../faraday/*.rb', __FILE__)].each{|f| require f}
require File.expand_path('../protractor/configuration', __FILE__)
require File.expand_path('../protractor/api', __FILE__)
require File.expand_path('../protractor/client', __FILE__)

module Protractor

  extend Configuration

  # Alias for Protractor::Client.new
  # @return [Protractor::Client]
  def self.client(options = {})
    Protractor::Client.new(options)
  end
end
