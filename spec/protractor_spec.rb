require 'spec_helper'

RSpec.describe Protractor do
  before(:each) { Protractor.reset }
  after { Protractor.reset }

  describe "#client" do
    it "should be a Protractor::Client" do
      expect(Protractor.client).to be_a(Protractor::Client)
    end
  end

  describe '#api_key=' do
    it 'should set the api key' do
      Protractor.api_key = 'test'
      expect(Protractor.api_key).to eq('test')
    end
  end

  describe '#api_version' do
    it 'should return the default api version' do
      expect(Protractor.api_version).to eq(Protractor::Configuration::DEFAULT_API_VERSION)
    end
  end

  describe '#api_version=' do
    it 'should set the api_version' do
      Protractor.api_version = '2.0'
      expect(Protractor.api_version).to eq('2.0')
    end
  end

  describe '#adapter' do
    it 'should return the default adapter' do
      expect(Protractor.adapter).to eq(Protractor::Configuration::DEFAULT_ADAPTER)
    end
  end

  describe '#adapter=' do
    it 'should set the adapter' do
      Protractor.adapter = :typhoeus
      expect(Protractor.adapter).to eq(:typhoeus)
    end
  end

  describe '#endpoint' do
    it 'should return the default endpoint' do
      expect(Protractor.endpoint).to eq(Protractor::Configuration::DEFAULT_ENDPOINT)
    end
  end

  describe '#endpoint=' do
    it 'should set the endpoint' do
      Protractor.endpoint = 'http://www.google.com'
      expect(Protractor.endpoint).to eq('http://www.google.com')
    end
  end

  describe '#test_mode=' do
    it 'should set test mode' do
      Protractor.test_mode = true
      expect(Protractor.test_mode).to eq(true)
    end

    it 'should change api_version' do
      Protractor.test_mode = false
      expect(Protractor.api_version).to eq('1.0')

      Protractor.test_mode = true
      expect(Protractor.api_version).to eq('Test')
    end
  end

  describe '#configure' do
    Protractor::Configuration::VALID_OPTIONS_KEYS.each do |key|

      it "should set the #{key}" do
        Protractor.configure do |config|
          config.send("#{key}=", key)
          expect(Protractor.send(key)).to eq(key)
        end
      end
    end
  end
end
