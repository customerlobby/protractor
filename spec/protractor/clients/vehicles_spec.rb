require 'spec_helper'

RSpec.describe Protractor::Client::Vehicles do
  before do
    Protractor.reset
    @client = Protractor::Client.new
    @startDate = Time.new(2015, 3, 25)
    @endDate = Time.new(2015,3,30);
  end

  describe 'vehicles' do
    it 'should require a range of dates' do
      expect { @client.vehicles }.to raise_error(ArgumentError)
    end

    it 'should retrieve a list of vehicles' do
      stub_request_with_fixture(:one_day_request,
        { startDate: @startDate, endDate: @endDate })
      vehicles = @client.vehicles(startDate: @startDate, endDate: @endDate)
      expect(vehicles.first["ID"]).to eq("ccdb63ef-c462-444a-8656-0d19f7aa4371")
    end

    it 'should return an empty array with no results' do
      stub_request_with_fixture(:empty_request,
        { startDate: @startDate, endDate: @endDate })
      vehicles = @client.vehicles(startDate: @startDate, endDate: @endDate)
      expect(vehicles).to eq([])
    end

    it 'should raise API errors' do
      stub_request_with_fixture(:error_request,
        { startDate: @startDate, endDate: @endDate })
      search = { startDate: @startDate, endDate: @endDate }
      expect { @client.vehicles(search) }.to raise_error(Protractor::Response::Error)
    end
  end
end

