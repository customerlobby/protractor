require 'spec_helper'

RSpec.describe Protractor::Client::Contacts do
  before do
    Protractor.reset
    @client = Protractor::Client.new
    stub_request_with_fixture(:one_day_request,
      { startDate: Time.new(2015, 3, 25), endDate: Time.now })
    stub_request_with_fixture(:empty_request,
      { startDate: Time.new(2013, 3, 25), endDate: Time.now })
    stub_request_with_fixture(:error_request,
      { startDate: Time.new(2015, 1, 1), endDate: Time.new(2015, 12, 31) })
  end

  describe 'contacts' do
    it 'should require a range of dates' do
      expect { @client.contacts }.to raise_error(ArgumentError)
    end

    it 'should retrieve a list of contacts' do
      contacts = @client.contacts(startDate: Time.new(2015, 3, 25))
      expect(contacts.first["FileAs"]).to eq("Test Contact")
    end

    it 'should return an empty array with no results' do
      contacts = @client.contacts(startDate: Time.new(2013, 3, 25))
      expect(contacts).to eq([])
    end

    it 'should raise API errors' do
      search = { startDate: Time.new(2015, 1, 1), endDate: Time.new(2015, 12, 31) }
      expect { @client.contacts(search) }.to raise_error(Protractor::Response::Error)
    end
  end
end
