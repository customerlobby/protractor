require 'spec_helper'

RSpec.describe Protractor::Client::Invoices do
  before do
    Protractor.reset
    @client = Protractor::Client.new
    stub_request_with_fixture(:one_day_request,
      { startDate: Time.new(2015, 3, 25), endDate: Time.now })
    stub_request_with_fixture(:empty_request,
      { startDate: Time.new(2013, 3, 25), endDate: Time.now })
    stub_request_with_fixture(:error_request,
      { startDate: Time.new(2015, 1, 1), endDate: Time.new(2015, 12, 31) })
  end

  describe 'invoices' do
    it 'should require a range of dates' do
      expect { @client.invoices }.to raise_error(ArgumentError)
    end

    it 'should retrieve a list of invoices' do
      invoices = @client.invoices(startDate: Time.new(2015, 3, 25))
      expect(invoices.first["ID"]).to eq("94b91992-0583-4e89-83d1-3a9377c56262")
    end

    it 'should return an empty array with no results' do
      invoices = @client.invoices(startDate: Time.new(2013, 3, 25))
      expect(invoices).to eq([])
    end

    it 'should raise API errors' do
      search = { startDate: Time.new(2015, 1, 1), endDate: Time.new(2015, 12, 31) }
      expect { @client.invoices(search) }.to raise_error(Protractor::Response::Error)
    end
  end
end
