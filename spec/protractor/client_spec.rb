require 'spec_helper'

RSpec.describe Protractor::Client do
  before do
    Protractor.reset
    @client = Protractor.client
  end

  it 'should connect using the configured endpoint and api version' do
    endpoint = @client.url_for({})
    connection = @client.send(:connection).build_url(nil).to_s
    expect(connection).to eq(endpoint)
  end

  it 'should gracefully fail on 500 errors' do
    Protractor.endpoint = "CRMServices.asmx?WSDL="
    stub_get(startDate: Time.new(2015, 3, 25), endDate: Time.now).to_return(
      body: fixture('wsdl_request.html'),
      headers: { content_type: "text/html" }
    )
    expect {
      @client.contacts(startDate: Time.new(2015, 3, 25))
    }.to raise_error(Protractor::Response::Error)
  end
end
